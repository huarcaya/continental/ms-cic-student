import { Controller, Get } from '@nestjs/common';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello() {
    return this.appService.getHello();
  }

  @Get(['/humans', '/humans.json', '/humans.txt'])
  getHumans() {
    return this.appService.getHumans();
  }
}
