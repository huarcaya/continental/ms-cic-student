import { Injectable } from '@nestjs/common';
import { HUMANS } from './commons/constants/humans.constant';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getHumans(): Humans {
    return HUMANS;
  }
}
