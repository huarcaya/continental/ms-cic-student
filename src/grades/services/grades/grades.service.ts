import { ClientProxy } from '@nestjs/microservices';
import { Inject, Injectable } from '@nestjs/common';

import { map } from 'rxjs/operators';

@Injectable()
export class GradesService {
  constructor(
    @Inject('MS_CIC_CORE') private readonly coreService: ClientProxy,
  ) {}

  async findByStudentAndLanguage(pidm: number, languageId: string) {
    const pattern = { cmd: 'student-language-grade' };
    const payload = { pidm, languageId };
    return this.coreService.send(pattern, payload).pipe(map((msg) => msg));
  }
}
