import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Logger, Param, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';

import { GradesService } from 'src/grades/services/grades/grades.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { PayloadTokenModel } from 'src/auth/models/payload-token.model';
import { RolesGuard } from 'src/auth/guards/roles.guard';

@ApiTags('grades')
@Controller('grades')
@UseGuards(JwtAuthGuard, RolesGuard)
export class GradesController {
  logger = new Logger();

  constructor(private readonly gradesService: GradesService) {}

  @Get('/language/:languageId')
  async findByStudentAndLanguage(
    @Param('languageId') languageId: string,
    @Req() req: Request,
  ) {
    console.log(req);
    const { pidm } = req.user as PayloadTokenModel;
    return await this.gradesService.findByStudentAndLanguage(pidm, languageId);
  }
}
