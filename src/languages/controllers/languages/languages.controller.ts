import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Req, UseGuards } from '@nestjs/common';

import { Request } from 'express';

import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { LanguagesService } from 'src/languages/services/languages/languages.service';
import { PayloadTokenModel } from 'src/auth/models/payload-token.model';
import { RolesGuard } from 'src/auth/guards/roles.guard';

@ApiTags('languages')
@Controller('languages')
@UseGuards(JwtAuthGuard, RolesGuard)
export class LanguagesController {
  constructor(private readonly languagesService: LanguagesService) {}

  @Get()
  async getLanguages(@Req() req: Request) {
    const { pidm } = req.user as PayloadTokenModel;

    return this.languagesService.findStudentLanguages(pidm);
  }
}
