import { ClientProxy } from '@nestjs/microservices';
import { Inject, Injectable } from '@nestjs/common';

import { map } from 'rxjs';

@Injectable()
export class LanguagesService {
  constructor(
    @Inject('MS_CIC_CORE') private readonly coreService: ClientProxy,
  ) {}

  async findLanguages() {
    const pattern = { cmd: 'get-languages' };
    const payload = {};

    return this.coreService.send(pattern, payload).pipe(map((msg) => msg));
  }

  async findStudentLanguages(pidm: number) {
    const pattern = { cmd: 'get-student-languages' };
    const payload = { pidm };

    return this.coreService.send(pattern, payload).pipe(map((msg) => msg));
  }
}
