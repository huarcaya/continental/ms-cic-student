import { Module } from '@nestjs/common';
import { LanguagesController } from './controllers/languages/languages.controller';
import { LanguagesService } from './services/languages/languages.service';

@Module({
  controllers: [LanguagesController],
  providers: [LanguagesService],
})
export class LanguagesModule {}
