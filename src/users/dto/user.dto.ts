import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
  Length,
} from 'class-validator';

export class CreateUserDto {
  @IsOptional()
  @IsPositive()
  @ApiProperty()
  readonly pidm: number;

  @IsString()
  @IsEmail()
  @ApiProperty()
  readonly email: string;

  @IsString()
  @ApiProperty()
  readonly username: string;

  @IsNotEmpty()
  @IsString()
  @Length(6)
  @ApiProperty()
  readonly password: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly firstName: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  readonly lastName: string;
}

export class UpdateUserDto extends PartialType(CreateUserDto) {}
