import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { DeleteResult, Repository } from 'typeorm';

import { CreateRoleDto, UpdateRoleDto } from 'src/users/dto/role.dto';
import { RoleEntity } from 'src/users/entities/role.entity';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(RoleEntity, 'pg')
    private roleRepository: Repository<RoleEntity>,
  ) {}

  async findAll(): Promise<RoleEntity[]> {
    return this.roleRepository.find();
  }

  async findOne(id: number): Promise<RoleEntity> {
    const role = await this.roleRepository.findOne({ where: { id } });

    if (!role) {
      throw new NotFoundException('Role not found');
    }

    return role;
  }

  async create(data: CreateRoleDto): Promise<RoleEntity> {
    const newRole = this.roleRepository.create(data);
    return this.roleRepository.save(newRole);
  }

  async update(id: number, data: UpdateRoleDto): Promise<RoleEntity> {
    const role = await this.roleRepository.findOne({ where: { id } });

    if (!role) {
      throw new NotFoundException('Role not found');
    }

    this.roleRepository.merge(role, data);

    return this.roleRepository.save(role);
  }

  async delete(id: number): Promise<DeleteResult> {
    return this.roleRepository.delete(id);
  }
}
