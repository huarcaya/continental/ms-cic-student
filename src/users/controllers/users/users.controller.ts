import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { CreateUserDto, UpdateUserDto } from 'src/users/dto/user.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { PayloadTokenModel } from 'src/auth/models/payload-token.model';
import { Request } from 'express';
import { RoleModel } from 'src/auth/models/role.model';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { UsersService } from 'src/users/services/users/users.service';

@ApiTags('users')
@Controller('users')
@UseGuards(JwtAuthGuard, RolesGuard)
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  @Roles(RoleModel.ADMIN)
  async findAll() {
    return this.usersService.findAll();
  }

  @Get('/profile')
  async userProfile(@Req() req: Request) {
    const user = req.user as PayloadTokenModel;
    return this.usersService.findByUsername(user.username);
  }

  @Get('/:id')
  @Roles(RoleModel.ADMIN)
  async findOne(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.findOne(id);
  }

  @Post()
  @Roles(RoleModel.ADMIN)
  async create(@Body() payload: CreateUserDto) {
    return this.usersService.create(payload);
  }

  @Put('/:id')
  @Roles(RoleModel.ADMIN)
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateUserDto,
  ) {
    return this.usersService.update(id, payload);
  }

  @Delete('/:id')
  @Roles(RoleModel.ADMIN)
  async delete(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.delete(id);
  }
}
