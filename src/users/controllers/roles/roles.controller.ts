import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { CreateRoleDto, UpdateRoleDto } from 'src/users/dto/role.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RoleModel } from 'src/auth/models/role.model';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { RolesService } from 'src/users/services/roles/roles.service';

@ApiTags('roles')
@Controller('roles')
@UseGuards(JwtAuthGuard, RolesGuard)
export class RolesController {
  constructor(private rolesService: RolesService) {}

  @Get()
  @Roles(RoleModel.ADMIN)
  async findAll() {
    return await this.rolesService.findAll();
  }

  @Get('/:id')
  @Roles(RoleModel.ADMIN)
  async findOne(@Param('id', ParseIntPipe) id: number) {
    return await this.rolesService.findOne(id);
  }

  @Post()
  @Roles(RoleModel.ADMIN)
  async create(@Body() payload: CreateRoleDto) {
    return await this.rolesService.create(payload);
  }

  @Put('/:id')
  @Roles(RoleModel.ADMIN)
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateRoleDto,
  ) {
    return await this.rolesService.update(id, payload);
  }

  @Delete('/:id')
  @Roles(RoleModel.ADMIN)
  async delete(@Param('id', ParseIntPipe) id: number) {
    return await this.rolesService.delete(id);
  }
}
