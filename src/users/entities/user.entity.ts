import { Exclude, Expose } from 'class-transformer';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserRoleEntity } from './user-role.entity';

@Entity({ database: 'pg', name: 'users', schema: 'cic' })
export class UserEntity {
  @PrimaryGeneratedColumn({ name: 'user_id' })
  id: number;

  @Column({ unique: true })
  pidm: number;

  @Column({ type: 'varchar', length: 255, unique: true })
  email: string;

  @Column({ type: 'varchar', length: 255, unique: true })
  username: string;

  @Exclude()
  @Column({ type: 'varchar', length: 255 })
  password: string;

  @Column({ name: 'first_name', type: 'varchar', length: 255 })
  firstName: string;

  @Column({ name: 'last_name', type: 'varchar', length: 255 })
  lastName: string;

  @Column({ name: 'is_enabled', default: true })
  isEnabled: boolean;

  @Column({ nullable: true })
  attempts?: number;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp without time zone',
    default: () => 'CURRENT_TIMESTAMP',
    select: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp without time zone',
    default: () => 'CURRENT_TIMESTAMP',
    select: false,
    nullable: false,
  })
  updatedAt?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    type: 'timestamp without time zone',
    select: false,
    nullable: true,
  })
  deletedAt?: Date;

  @Exclude()
  @OneToMany(() => UserRoleEntity, (userRole) => userRole.user)
  roles: UserRoleEntity[];

  @Expose()
  get authorities() {
    if (this.roles) {
      return this.roles
        .filter((item) => !!item)
        .map((item) => ({
          ...item.role,
        }));
    }
  }

  /*
   * It doesn't work when we activate serializers using
   * `app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));`
   * in `main.ts`
   */
  /*toJSON() {
    delete this.password;
    return this;
  }*/
}
