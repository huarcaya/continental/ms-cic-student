import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigType } from '@nestjs/config';
import { Global, Module } from '@nestjs/common';

import config from 'src/config';

@Global()
@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: 'MS_CIC_META',
        inject: [config.KEY],
        useFactory: (configService: ConfigType<typeof config>) => {
          const { host, port } = configService.cicMetaService;
          return {
            transport: Transport.TCP,
            options: { host, port },
          };
        },
      },
      {
        name: 'MS_CIC_CORE',
        inject: [config.KEY],
        useFactory: (configService: ConfigType<typeof config>) => {
          const { host, port } = configService.cicCoreService;
          return {
            transport: Transport.TCP,
            options: { host, port },
          };
        },
      },
    ]),
  ],
  exports: [ClientsModule],
})
export class MicrosModule {}
