interface Developer {
  name: string;
  role: string;
  email: string;
  site: string;
}

interface DeveloperThanks {
  from: string;
  to: string[];
}

interface SiteInfo {
  standards: string[];
  components: string[];
  software: string[];
}

interface Humans {
  team: Developer[];
  thanks: DeveloperThanks[];
  site: SiteInfo;
}
