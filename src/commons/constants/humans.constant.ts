export const HUMANS: Humans = {
  team: [
    {
      name: 'Gustavo Huarcaya',
      role: 'Full-stack',
      email: 'diavolo[at]gahd.net',
      site: 'https://gahd.net',
    },
  ],
  thanks: [
    {
      from: 'Gustavo Huarcaya',
      to: ['Yhuval', 'Daniel', 'Amy'],
    },
  ],
  site: {
    standards: ['JSON'],
    components: ['NestJS', 'PostgreSQL', 'MSSQL'],
    software: ['DBeaver', 'Fork', 'Insomnia', 'vscode'],
  },
};
