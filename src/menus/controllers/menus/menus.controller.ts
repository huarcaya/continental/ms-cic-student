import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, UseGuards } from '@nestjs/common';

import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { MenusService } from 'src/menus/services/menus/menus.service';
import { RolesGuard } from 'src/auth/guards/roles.guard';

@ApiTags('menus')
@Controller('menus')
@UseGuards(JwtAuthGuard, RolesGuard)
export class MenusController {
  constructor(private readonly menusService: MenusService) {}

  @Get()
  async findAsync() {
    return this.menusService.findAllAsync();
  }
}
