import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

import { map } from 'rxjs';

@Injectable()
export class MenusService {
  constructor(
    @Inject('MS_CIC_META') private readonly metaService: ClientProxy,
  ) {}

  async findAllAsync() {
    const pattern = { cmd: 'cic-menu' };
    const payload = {};
    return this.metaService.send(pattern, payload).pipe(map((msg) => msg));
  }
}
