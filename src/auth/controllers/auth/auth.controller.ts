import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Post, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';

import { AuthService } from 'src/auth/services/auth/auth.service';
import { UserEntity } from 'src/users/entities/user.entity';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/login')
  @UseGuards(AuthGuard('local'))
  login(@Req() req: Request) {
    const user = req.user as UserEntity;

    return this.authService.generateJwt(user);
  }

  @Post('/refresh')
  @UseGuards(AuthGuard('refresh-jwt'))
  refresh(@Req() req: Request) {
    const user = req.user as UserEntity;

    return this.authService.regenerateJwt(user);
  }
}
