export enum RoleModel {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER',
  STUDENT = 'ROLE_STUDENT',
}
