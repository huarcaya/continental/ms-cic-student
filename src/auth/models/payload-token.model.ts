export interface PayloadTokenModel {
  pidm: number;
  firstName: string;
  lastName: string;
  username: string;
  authorities: any;
  sub: number;
}
