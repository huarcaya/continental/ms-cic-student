import { RefreshJwtStrategy } from './refresh-jwt.strategy';

describe('RefreshJwtStrategy', () => {
  it('should be defined', () => {
    expect(new RefreshJwtStrategy()).toBeDefined();
  });
});
