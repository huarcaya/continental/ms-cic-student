import { ConfigType } from '@nestjs/config';
import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PayloadTokenModel } from 'src/auth/models/payload-token.model';
import * as bcrypt from 'bcrypt';

import { UserEntity } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/services/users/users.service';
import config from 'src/config';

@Injectable()
export class AuthService {
  constructor(
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
    private jwtService: JwtService,
    private usersService: UsersService,
  ) {}

  async validateUser(username: string, password: string): Promise<UserEntity> {
    const user = await this.usersService.findByUsername(username);

    if (user) {
      const isMatch = await bcrypt.compare(password, user.password);

      if (isMatch) {
        return user;
      }
    }

    return null;
  }

  generateJwt(user: UserEntity) {
    const authorities = user.roles
      .filter((item) => !!item)
      .map((item) =>
        // ({ ...item.role, })
        {
          const role = { ...item.role };
          return role.name;
        },
      );

    const payload: PayloadTokenModel = {
      pidm: user.pidm,
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      authorities,
      sub: user.id,
    };

    const { id, password, roles, ...rest } = user;

    return {
      access_token: this.jwtService.sign(payload),
      refresh_token: this.jwtService.sign(payload, {
        secret: this.configService.jwtRefreshSecret,
        expiresIn: this.configService.jwtRefreshExp,
      }),
      user: {
        ...rest,
        authorities,
      },
    };
  }

  regenerateJwt(user: UserEntity) {
    const payload: PayloadTokenModel = {
      pidm: user.pidm,
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      authorities: user.authorities,
      sub: user.id,
    };

    const { id, password, roles, ...rest } = user;

    return {
      access_token: this.jwtService.sign(payload),
      refresh_token: this.jwtService.sign(payload, {
        secret: this.configService.jwtRefreshSecret,
        expiresIn: this.configService.jwtRefreshExp,
      }),
      user: {
        ...rest,
        authorities: user.authorities,
      },
    };
  }
}
