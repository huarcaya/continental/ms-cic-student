import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';

import { PayloadTokenModel } from '../models/payload-token.model';
import { RoleModel } from '../models/role.model';
import { ROLES_KEY } from '../decorators/roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  private readonly logger = new Logger(RolesGuard.name);

  constructor(private reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const roles = this.reflector.get<RoleModel[]>(
      ROLES_KEY,
      context.getHandler(),
    );

    if (!roles) {
      return true;
    }

    // [ 'ROLE_ADMIN' ]
    const request = context.switchToHttp().getRequest();
    const user = request.user as PayloadTokenModel;
    // [ 'ROLE_ADMIN', 'ROLE_USER' ]
    const hasRole = roles.some((role) => user.authorities.includes(role));
    if (!hasRole) {
      this.logger.warn(
        JSON.stringify(user.authorities),
        'UnauthorizedException',
      );
      throw new UnauthorizedException(
        'The user does not have enough privileges to do this kind of operation.',
      );
    }
    return true;
  }
}
