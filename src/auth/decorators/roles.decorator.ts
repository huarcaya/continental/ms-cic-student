import { SetMetadata } from '@nestjs/common';

import { RoleModel } from '../models/role.model';

export const ROLES_KEY = 'roles';

export const Roles = (...args: RoleModel[]) => SetMetadata(ROLES_KEY, args);
